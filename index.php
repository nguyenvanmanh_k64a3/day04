<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Information Form</title>
</head>
<style><?php include "./style.css"; ?></style>
<script src="https://cdn.tailwindcss.com"></script>
<script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
<body>

    <?php
        $reg = "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/";
        $error = array();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["name"])) {
                array_push($error,"Hãy nhập tên.");
            } 
            if (empty($_POST["gender"])) {
                array_push($error,"Hãy chọn giới tính.");
            } 
            if ($_POST["department"] == "None") {
                array_push($error,"Hãy chọn phân khoa.");
            }
            if (empty($_POST["birthday"])) {
                array_push($error,"Hãy nhập ngày sinh.");
            } elseif (!preg_match($reg,$_POST["birth"])) {
                array_push($error,"Hãy nhập ngày sinh đúng định dạng.");
            }
        }
    ?>

    <div class="w-screen h-screen flex justify-center items-center text-md">
        <div class="w-fit border-2 border-cyan-700 pt-[70px] pl-[47px] pb-[38px] pr-[35px]">
            <h5 class="text-red-600">Hãy nhập tên.</h5>
            <h5 class="text-red-600">Hãy nhập ngày sinh</h5>

            <?php
                foreach($error as $value){
                    echo "<h5 class='text-red-300 text-sm underline decoration-pink-500'> $value </h5>";
                }
            ?>

            <form
                method="POST"
                action=''
                class="flex flex-col justify-around pt-[20px] h-[360px]"
            >
                <!-- Name -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="alumnus-name" class="leading-none after:content-['*'] after:text-red-600">
                                Họ và tên
                        </label>
                    </div>
                    <div>
                        <input 
                            class="ml-[20px] w-[320px] border-2 border-cyan-700 h-[40px] pl-[10px]"
                            type="text"
                            name="name"
                            id="alumnus-name" 
                        >
                    </div>
                </div>

                <!-- Gender -->
                <div class="flex h-[40px]">
                    <div class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]">
                        <label for="gender" class="leading-none after:content-['*'] after:text-red-600">
                            Giới tính
                        </label>
                    </div>
                    <div class="flex w-[160px] justify-between ml-[20px]">
                        <div class="flex items-center">
                            <input 
                                class="appearance-none border-2 border-cyan-700 checked:bg-green-600 pl-[10px]"
                                type="radio" 
                                name="gender"
                                id="gender"
                            >
                            <span>Nam</span>
                        </div>
                        
                        <div class="flex items-center">
                            <input
                                class="appearance-none border-2 border-cyan-700 checked:bg-green-600 pl-[10px]"
                                type="radio" 
                                name="gender"
                                id="gender"
                            >
                            <span>Nữ</span>
                        </div>
                    </div>
                </div>

                <!-- Department -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="department" class="leading-none after:content-['*'] after:text-red-600">
                                Phân khoa
                        </label>
                    </div>

                    <div>
                        <select id="department" name="department" class="w-[160px] ml-[20px] border-2 border-cyan-700 h-full">
                            <option value="None" selected></option>
                            <option value="MAT">Khoa học máy tính</option>
                            <option value="KDL">Khoa học vật liệu</option>
                        </select>
                    </div>
                    
                </div>

                <!-- Birthday -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="birthday" class="leading-none after:content-['*'] after:text-red-600">
                                Ngày sinh
                        </label>
                    </div>
                    <div>
                        <input
                            datepicker
                            class="ml-[20px] w-[160px] border-2 border-cyan-700 h-[40px] pl-[10px]"
                            type="text"
                            name="birthday"
                            id="birthday"
                            datepicker-format="dd/mm/yyyy"
                            placeholder="dd/mm/yyyy"
                        >
                    </div>
                </div>

                <!-- Address -->
                <div class="flex h-[40px]">
                    <div 
                        class="flex h-full w-[120px] justify-center items-center bg-green-500 px-[12px]"
                    >
                        <label for="address" class="leading-none after:content-['*'] after:text-red-600">
                                Địa chỉ
                        </label>
                    </div>
                    <div>
                        <input
                            class="ml-[20px] w-[320px] border-2 border-cyan-700 h-[40px] pl-[10px]"
                            type="text"
                            name="address"
                            id="address"
                        >
                    </div>
                </div>

                
                <!-- Button Submit -->
                <div class="flex justify-center items-center pt-[30px]">
                    <button class="w-[120px] h-[48px] bg-green-500 rounded-md border-2 border-cyan-700" type="submit">
                        Đăng ký
                    </button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>